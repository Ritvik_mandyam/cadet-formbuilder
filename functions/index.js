// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp();

const express = require('express');
const cors = require('cors');

const app = express();
const db = admin.firestore();
var whitelist = ['https://dftly-tyro.firebaseapp.com', 'http://dftly-tyro.firebaseapp.com'];
var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
};

// Automatically allow cross-origin requests
app.use(cors({ corsOptions }));

// build multiple CRUD interfaces:
app.get('/tags/search/:org/:input', (req, res) => {
  const results = [];
  db.collection(`tyro/${req.params['org']}/tags`).get()
    .then(snapshot => {
      snapshot.forEach(doc => {
        if (doc.id.toLowerCase().includes(req.params['input'])) {
          results.push(doc.id);
        }
      });
      res.send(results);
    })
});

app.get('/tags/list/:org', (req, res) => {
  const results = [];
  db.collection(`tyro/${req.params['org']}/tags`).get()
    .then(snapshot => {
      snapshot.forEach(doc => {
        results.push(doc.id);
      });
      res.send(results);
    })
});

// Expose Express API as a single Cloud Function:
exports.tyro = functions.https.onRequest(app);
