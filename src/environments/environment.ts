// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAy9D9S4exr0dozBDiqCkXv4IGFkhKd7SE',
    authDomain: 'dftly-messaging.firebaseapp.com',
    databaseURL: 'https://dftly-messaging.firebaseio.com',
    projectId: 'dftly-messaging',
    storageBucket: 'dftly-messaging.appspot.com',
    messagingSenderId: '479229989896',
    appId: '1:479229989896:web:0c279695f8f46482'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
