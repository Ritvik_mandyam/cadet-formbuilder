import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {MatAutocompleteSelectedEvent, MatChipInputEvent} from '@angular/material';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {AutocompleteService} from '../autocomplete.service';

@Component({
  selector: 'app-tag-autocomplete',
  templateUrl: './tag-autocomplete.component.html',
  styleUrls: ['./tag-autocomplete.component.css']
})
export class TagAutocompleteComponent implements OnInit {
  _allTags: any[];
  @ViewChild('tagsInput') private tagsInput: ElementRef;

  _tags: any[] = [];
  _filteredOptions: any[] = [];
  _deletedTags: any[] = [];
  _newTags: any[] = [];

  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  @Input() org: string;
  @Output() tagsChange = new EventEmitter();
  @Output() deletedTagsChange = new EventEmitter();
  @Output() newTagsChange = new EventEmitter();

  newTag = '';

  @Input()
  get tags() {
    return this._tags;
  }

  set tags(tags: any[]) {
    this._tags = tags;
    this.tagsChange.emit(this._tags);
  }

  @Input()
  get deletedTags() {
    return this._deletedTags;
  }

  set deletedTags(deletedTags: any[]) {
    if (!deletedTags) {
      deletedTags = [];
    }
    this._deletedTags = deletedTags;
    this.deletedTagsChange.emit(this._deletedTags);
  }

  @Input()
  get newTags() {
    return this._newTags;
  }

  set newTags(newTags: any[]) {
    if (!newTags) {
      newTags = [];
    }
    this._newTags = newTags;
    this.newTagsChange.emit(this.newTags);
  }

  constructor(private autocomplete: AutocompleteService) {
  }

  ngOnInit() {
    this.autocomplete.org = this.org;
    this.autocomplete.list().subscribe(tags => {
      this._allTags = tags;
    });
  }

  addTag(event: MatChipInputEvent): void {
    const input = event.input;
    const _value = event.value;

    if ((_value || '').trim()) {
      if (!this.tags) { this.tags = []; }
      this.tags.push(_value.trim());
      this.tags = [...this.tags];

      const deletedIndex = this.deletedTags.indexOf(_value.trim());
      if (deletedIndex > -1) {
        this.deletedTags.splice(deletedIndex, 1);
        this.deletedTags = [...this.deletedTags];
      }

      if (!this._allTags.includes(_value.trim())) {
        this.newTags.push(_value.trim());
        this.newTags = [...this.newTags];
      }
    }

    // Reset the input value
    if (input) {
      input.value = '';
      this.newTag = '';
    }
  }

  removeTag(tag): void {
    const index = this.tags.indexOf(tag);

    if (index >= 0) {
      this.tags.splice(index, 1);
      this.tags = [...this.tags];

      const newTagIndex = this.newTags.indexOf(tag);
      if (newTagIndex >= 0) {
        this.newTags.splice(newTagIndex, 1);
        this.newTags = [...this.newTags];
      }

      if (!this.deletedTags.includes(tag)) {
        this.deletedTags.push(tag);
        this.deletedTags = [...this.deletedTags];
      }
    }
  }

  filterTags() {
    this._filteredOptions = this.autocomplete.search(this.newTag, this.tags, this._allTags);
  }

  tagAutocomplete(event: MatAutocompleteSelectedEvent) {
    this.newTag = '';
    this.tagsInput.nativeElement.value = '';
    if (!this.tags) { this.tags = []; }
    this.tags.push(event.option.viewValue.trim());
    this.tags = [...this.tags];
  }
}
