import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor(public auth: AuthService, private db: AngularFirestore) {
  }

  getType(object: any) {
    if (typeof object === 'object') {
      if (object != null) {
        if (Array.isArray(object)) {
          return 'array';
        } else if (object instanceof Date) {
          return 'date';
        } else {
          return 'object';
        }
      } else {
        return 'null';
      }
    } else {
      return typeof object;
    }
  }

  getKeys(object: any) {
    if (object) {
      return Object.keys(object);
    } else {
      return null;
    }
  }
}
