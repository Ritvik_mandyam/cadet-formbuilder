import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsListPageComponent } from './forms-list-page.component';

describe('FormsListPageComponent', () => {
  let component: FormsListPageComponent;
  let fixture: ComponentFixture<FormsListPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormsListPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormsListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
