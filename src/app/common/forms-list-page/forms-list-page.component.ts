import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthService} from '../auth.service';
import * as firebase from 'firebase';
import {ActivatedRoute, Router} from '@angular/router';
import {AngularFirestore} from '@angular/fire/firestore';
import {MatTable} from '@angular/material';

@Component({
  selector: 'app-forms-list-page',
  templateUrl: './forms-list-page.component.html',
  styleUrls: ['./forms-list-page.component.css']
})
export class FormsListPageComponent implements OnInit {
  params: any;
  forms: any[] = [];
  displayedColumns: string[] = ['name'];
  @ViewChild(MatTable) table: MatTable<any>;

  constructor(public auth: AuthService, private route: ActivatedRoute, private router: Router, private db: AngularFirestore) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.params = params;
    });
    this.auth.user
      .subscribe((user: firebase.User) => {
        if (user) {
          this.db.doc(`tyro/${this.params['org']}/users/${user.uid}`).get()
            .subscribe((doc) => {
              if (doc.exists) {
                this.forms = doc.data()['createdForms'].map(form => {
                  form['id'] = form['reference']['id'];
                  return form;
                });
              }
            });
        } else {
          this.router.navigateByUrl(`/organization/${this.params['org']}/login`);
        }
      });
  }

}
