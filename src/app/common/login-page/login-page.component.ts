import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  email: string;
  password: string;
  params: any;

  constructor(public authService: AuthService, private route: ActivatedRoute, private router: Router) { }

  login() {
    this.authService.login(this.params['org'] + '--' + this.email, this.password)
      .then(value => {
        console.log(value);
        this.router.navigateByUrl(`/organization/${this.params['org']}`);
      })
      .catch(err => {
        alert('Something went wrong: ' + err.message);
      });
  }

  logout() {
    this.email = this.password = '';
    this.authService.logout();
  }

  register() {
    this.authService.doRegister(this.params['org'] + '--' + this.email, this.password)
      .then(res => {
        console.log(res);
        alert('Your account has been created. Please log in now.');
      }, err => {
        console.log(err);
        alert('Something went wrong: ' + err.message);
      });
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.params = params;
    });
  }

}
