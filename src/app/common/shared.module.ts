import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ToolbarComponent} from './toolbar/toolbar.component';
import {FormsListPageComponent} from './forms-list-page/forms-list-page.component';
import {LoginPageComponent} from './login-page/login-page.component';
import {RegistrationThanksPageComponent} from './registration-thanks-page/registration-thanks-page.component';
import {
  MatAutocompleteModule,
  MatButtonModule, MatCardModule,
  MatChipsModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatListModule,
  MatTableModule,
  MatToolbarModule
} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import { TagAutocompleteComponent } from './tag-autocomplete/tag-autocomplete.component';
import { TagManagementPageComponent } from './tag-management-page/tag-management-page.component';

@NgModule({
  declarations: [
    ToolbarComponent,
    FormsListPageComponent,
    LoginPageComponent,
    RegistrationThanksPageComponent,
    TagAutocompleteComponent,
    TagManagementPageComponent
  ],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    RouterModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatCardModule,
    MatListModule
  ],
  exports: [
    ToolbarComponent,
    FormsListPageComponent,
    LoginPageComponent,
    RegistrationThanksPageComponent,
    TagAutocompleteComponent
  ]
})
export class SharedModule { }
