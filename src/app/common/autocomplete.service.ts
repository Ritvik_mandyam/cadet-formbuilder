import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AutocompleteService {
  protected listUrl = 'https://us-central1-dftly-messaging.cloudfunctions.net/tyro/tags/list';
  protected _org;

  constructor(private http: HttpClient) {
  }

  set org(org: string) {
    this._org = org;
    this.list();
  }

  get org() {
    return this._org;
  }

  search(input: string, existingTags: string[], allTags: string[]): string[] {
    if (!this._org) {
      throw Error('Please set an org first.');
    }
    if (!existingTags) {
      existingTags = [];
    }
    return allTags.filter((tag: string) => tag.toLowerCase().includes(input.toLowerCase()) && !existingTags.includes(tag));
  }
  list(): Observable<any> {
    if (!this._org) {
      throw Error('Please set an org first.');
    }
    return this.http.get(`${this.listUrl}/${this.org}`);
  }
}
