import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AngularFirestore} from '@angular/fire/firestore';
import {UtilService} from '../util.service';

@Component({
  selector: 'app-tag-management-page',
  templateUrl: './tag-management-page.component.html',
  styleUrls: ['./tag-management-page.component.css']
})
export class TagManagementPageComponent implements OnInit {
  params: any;
  tags: any[] = [];
  expandedTag: any = null;
  constructor(private route: ActivatedRoute, private router: Router, private db: AngularFirestore, public util: UtilService) {
  }

  ngOnInit() {
    this.route.params
      .subscribe(params => {
        this.params = params;
        this.db.collection(`tyro/${params['org']}/tags`).get()
          .subscribe(tags => {
            tags.forEach(tag => {
              this.tags.push({
                'name': tag.id,
                'data': tag.data(),
              });
            });
          });
      });
  }
}
