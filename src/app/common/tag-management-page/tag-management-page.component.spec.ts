import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TagManagementPageComponent } from './tag-management-page.component';

describe('TagManagementPageComponent', () => {
  let component: TagManagementPageComponent;
  let fixture: ComponentFixture<TagManagementPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagManagementPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagManagementPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
