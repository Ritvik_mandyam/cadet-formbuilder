import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationThanksPageComponent } from './registration-thanks-page.component';

describe('RegistrationThanksPageComponent', () => {
  let component: RegistrationThanksPageComponent;
  let fixture: ComponentFixture<RegistrationThanksPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrationThanksPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationThanksPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
