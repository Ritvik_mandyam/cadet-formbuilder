import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../auth.service';
import {AngularFirestore} from '@angular/fire/firestore';
import * as firebase from 'firebase';

@Component({
  selector: 'app-registration-thanks-page',
  templateUrl: './registration-thanks-page.component.html',
  styleUrls: ['./registration-thanks-page.component.css']
})
export class RegistrationThanksPageComponent implements OnInit {
  params: any;

  constructor(private route: ActivatedRoute, private router: Router, private auth: AuthService, private db: AngularFirestore) {
    route.params.subscribe((params) => this.params = params);
  }

  ngOnInit() {
    this.auth.user.subscribe((user: firebase.User) => {
      if (user) {
        this.db.doc(`tyro/${this.params['org']}/users/${user.uid}`).get()
          .subscribe((doc) => {
            if (!doc.exists) {
              // User is logged in, but no document in database.
              const userObject = {
                displayName: user.displayName,
                email: user.email,
                uid: user.uid
              };
              this.db.doc(`tyro/${this.params['org']}/users/${user.uid}`).set(userObject);
            } else {
              // User is logged in and exists in database.
              if (doc.data()['isAdmin']) {
                this.router.navigateByUrl(`/organization/${this.params['org']}`);
              } else {
                alert('User is not admin!');
                this.router.navigateByUrl(`/organization/${this.params['org']}/login`);
              }
            }
          });
      } else {
        // User is not logged in
        this.router.navigateByUrl(`/organization/${this.params['org']}/login`);
      }
    });
  }

}
