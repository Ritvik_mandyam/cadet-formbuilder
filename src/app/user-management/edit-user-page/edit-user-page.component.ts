import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AngularFirestore} from '@angular/fire/firestore';
import * as firebase from 'firebase';
import {UtilService} from '../../common/util.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user-page.component.html',
  styleUrls: ['./edit-user-page.component.css']
})
export class EditUserPageComponent implements OnInit {
  params: any;
  user: any = {tags: []};
  deletedTags: any[] = [];
  newTags: any[] = [];
  isNewUser = false;

  constructor(private route: ActivatedRoute, private router: Router, private db: AngularFirestore, private util: UtilService) {
    route.params.subscribe(params => this.params = params);
  }

  ngOnInit() {
    if (this.params.user) {
      this.db.doc(`tyro/${this.params['org']}/users/${this.params['user']}`).get()
        .subscribe(doc => {
          if (doc.exists) {
            this.user = doc.data();
            this.user['email'] = this.user['email'].includes('--') ? this.user['email'].split('--')[1] : this.user['email'];
          } else {
            alert('User not found!');
          }
        });
    } else {
      this.db.doc(`tyro/${this.params['org']}/users/blankUser`).get()
        .subscribe(blankUser => {
          if (blankUser.exists) {
            this.user = blankUser.data();
            this.isNewUser = true;
          } else {
            alert('No blank user template found!');
          }
        });
    }
    if (!this.user.tags) {
      this.user.tags = [];
    }
  }

  saveUsers() {
    let userDoc;
    if (!this.isNewUser) {
      userDoc = this.db.doc(`tyro/${this.params['org']}/users/${this.params['user']}`);
    } else {
      const id = this.db.createId();
      userDoc = this.db.doc(`tyro/${this.params['org']}/users/${id}`);
    }

    const promises = [];
    const userObject = {
      'reference': userDoc.ref,
      'humanReadableName': this.user.displayName || this.user.email
    };

    for (const tag of this.deletedTags) {
      promises.push(this.db.doc(`tyro/${this.params['org']}/tags/${tag}`)
        .update({users: firebase.firestore.FieldValue.arrayRemove(userObject)}));
    }

    for (const tag of this.newTags) {
      promises.push(this.db.doc(`tyro/${this.params['org']}/tags/${tag}`).set({users: [userObject]}));
    }

    for (const tag of this.user['tags']) {
      promises.push(this.db.doc(`tyro/${this.params['org']}/tags/${tag}`)
        .update({users: firebase.firestore.FieldValue.arrayUnion(userObject)}));
    }

    Promise.all(promises)
      .then(() => {
        this.user['email'] = !this.user['email'].includes('--') ? this.params['org'] + '--' + this.user['email'] : this.user['email'];
        userDoc.set(this.user)
          .then(() => {
            if (this.isNewUser) {
              this.router.navigateByUrl(`/organization/${this.params['org']}/user/${userDoc.ref.id}`);
            } else {
              this.user['email'] = this.user['email'].includes('--') ? this.user['email'].split('--')[1] : this.user['email'];
            }
          });
      });
  }
}
