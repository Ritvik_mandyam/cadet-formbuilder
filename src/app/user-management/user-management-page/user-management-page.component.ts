import {Component, OnInit} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {ActivatedRoute, Params} from '@angular/router';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {AuthService} from '../../common/auth.service';

@Component({
  selector: 'app-user-management-page',
  templateUrl: './user-management-page.component.html',
  styleUrls: ['./user-management-page.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class UserManagementPageComponent implements OnInit {
  constructor(private db: AngularFirestore, private route: ActivatedRoute, public auth: AuthService) {
    route.params.subscribe((params) => {
      this.params = params;
    });
  }

  dataSource = [];
  displayedColumns: string[] = ['displayName', 'email', 'isAdmin', 'uid'];
  private params: Params;

  ngOnInit() {
    this.db.collection(`tyro/${this.params['org']}/users`).valueChanges({idField: 'uid'})
      .subscribe((users) => {
        // TODO: HACK ALERT! The Material table only updates when a new object is assigned to the datasource.
        this.dataSource = users.filter(user => user.uid !== 'blankUser');
      });
  }
}
