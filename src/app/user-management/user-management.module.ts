import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserManagementPageComponent} from './user-management-page/user-management-page.component';
import {
  MatButtonModule,
  MatCardModule, MatCheckboxModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatSlideToggleModule,
  MatTableModule, MatTooltipModule
} from '@angular/material';
import {SharedModule} from '../common/shared.module';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import { EditUserPageComponent } from './edit-user-page/edit-user-page.component';

@NgModule({
  declarations: [UserManagementPageComponent, EditUserPageComponent],
  imports: [
    CommonModule,
    MatCardModule,
    SharedModule,
    MatTableModule,
    FormsModule,
    MatInputModule,
    MatSlideToggleModule,
    MatListModule,
    RouterModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    MatCheckboxModule
  ]
})
export class UserManagementModule { }
