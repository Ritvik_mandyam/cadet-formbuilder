import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FormBuilderComponent} from './form-builder/form-builder/form-builder.component';
import {LoginPageComponent} from './common/login-page/login-page.component';
import {FormsListPageComponent} from './common/forms-list-page/forms-list-page.component';
import {RegistrationThanksPageComponent} from './common/registration-thanks-page/registration-thanks-page.component';
import {UserManagementPageComponent} from './user-management/user-management-page/user-management-page.component';
import {EditUserPageComponent} from './user-management/edit-user-page/edit-user-page.component';


const routes: Routes = [
  {path: 'organization/:org/form', component: FormBuilderComponent},
  {path: 'organization/:org/form/:form', component: FormBuilderComponent},
  {path: 'organization/:org', component: FormsListPageComponent},
  {path: '', pathMatch: 'full', redirectTo: 'organization/default'},
  {path: 'organization/:org/login', component: LoginPageComponent},
  {path: 'organization/:org/thanks', component: RegistrationThanksPageComponent},
  {path: 'organization/:org/users', component: UserManagementPageComponent},
  {path: 'organization/:org/user', component: EditUserPageComponent},
  {path: 'organization/:org/user/:user', component: EditUserPageComponent},
  // {path: 'organization/:org/tags', component: TagManagementPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
