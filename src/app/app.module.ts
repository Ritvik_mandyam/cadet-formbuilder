import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {DndModule} from 'ngx-drag-drop';
import {SweetAlert2Module} from '@toverux/ngx-sweetalert2';
import {environment} from '../environments/environment';
import {AngularFireModule} from '@angular/fire';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AuthService} from './common/auth.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {UserManagementModule} from './user-management/user-management.module';
import {FormBuilderModule} from './form-builder/form-builder.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    SweetAlert2Module.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    DndModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    UserManagementModule,
    FormBuilderModule
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
