import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormBuilderComponent} from './form-builder/form-builder.component';
import {FormFieldComponent} from './form-field/form-field.component';
import {
  MatAutocompleteModule,
  MatButtonModule, MatCardModule, MatCheckboxModule,
  MatChipsModule,
  MatDividerModule,
  MatFormFieldModule,
  MatIconModule, MatInputModule, MatRadioModule, MatSelectModule,
  MatSidenavModule, MatSlideToggleModule
} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {DndModule} from 'ngx-drag-drop';
import {SharedModule} from '../common/shared.module';
import {HttpClientModule} from '@angular/common/http';
import {ContenteditableModule} from '@ng-stack/contenteditable';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [
    FormBuilderComponent,
    FormFieldComponent,
  ],
  imports: [
    CommonModule,
    ContenteditableModule,
    MatButtonModule,
    MatSidenavModule,
    FormsModule,
    MatDividerModule,
    DndModule,
    MatChipsModule,
    MatIconModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatCardModule,
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule,
    MatRadioModule,
    MatSlideToggleModule,
    HttpClientModule,
    SharedModule,
    RouterModule
  ],

})
export class FormBuilderModule { }
