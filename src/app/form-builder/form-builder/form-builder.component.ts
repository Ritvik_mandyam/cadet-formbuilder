import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {DndDropEvent, DropEffect} from 'ngx-drag-drop';
import {field} from '../global.model';
import {ActivatedRoute, Router} from '@angular/router';
import swal from 'sweetalert2';
import {AngularFirestore, DocumentReference} from '@angular/fire/firestore';
import {AuthService} from '../../common/auth.service';
import * as firebase from 'firebase';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {UtilService} from '../../common/util.service';
import FieldValue = firebase.firestore.FieldValue;

@Component({
  selector: 'app-edit-app',
  templateUrl: './form-builder.component.html',
  styleUrls: ['./form-builder.component.css']
})
export class FormBuilderComponent implements OnInit {
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  newTag = '';

  fieldModels: Array<field> = [
    {
      'type': 'text',
      'icon': 'fa-font',
      'label': 'Text',
      'description': 'Enter your name',
      'placeholder': 'Enter your name',
      'className': 'form-control',
      'subtype': 'text',
      'regex': '',
      'handle': true
    },
    {
      'type': 'email',
      'icon': 'fa-envelope',
      'required': true,
      'label': 'Email',
      'description': 'Enter your email',
      'placeholder': 'Enter your email',
      'className': 'form-control',
      'subtype': 'text',
      'regex': '^([a-zA-Z0-9_.-]+)@([a-zA-Z0-9_.-]+)\.([a-zA-Z]{2,5})$',
      'errorText': 'Please enter a valid email',
      'handle': true
    },
    {
      'type': 'phone',
      'icon': 'fa-phone',
      'label': 'Phone',
      'description': 'Enter your phone',
      'placeholder': 'Enter your phone',
      'className': 'form-control',
      'subtype': 'text',
      'regex': '^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$',
      'errorText': 'Please enter a valid phone number',
      'handle': true
    },
    {
      'type': 'number',
      'label': 'Number',
      'icon': 'fa-html5',
      'description': 'Age',
      'placeholder': 'Enter your age',
      'className': 'form-control',
      'value': '20',
      'min': 12,
      'max': 90
    },
    {
      'type': 'date',
      'icon': 'fa-calendar',
      'label': 'Date',
      'placeholder': 'Date',
      'className': 'form-control'
    },
    {
      'type': 'datetime-local',
      'icon': 'fa-calendar',
      'label': 'DateTime',
      'placeholder': 'Date Time',
      'className': 'form-control'
    },
    {
      'type': 'textarea',
      'icon': 'fa-text-width',
      'label': 'Textarea'
    },
    {
      'type': 'paragraph',
      'icon': 'fa-paragraph',
      'label': 'Paragraph',
      'placeholder': 'Type your text to display here only',
      'pollAllowed': true
    },
    {
      'type': 'checkbox',
      'required': true,
      'label': 'Checkbox',
      'icon': 'fa-list',
      'description': 'Checkbox',
      'inline': true,
      'values': [
        {
          'label': 'Option 1',
          'value': 'option-1'
        },
        {
          'label': 'Option 2',
          'value': 'option-2'
        }
      ]
    },
    {
      'type': 'radio',
      'icon': 'fa-list-ul',
      'label': 'Radio',
      'description': 'Radio boxes',
      'values': [
        {
          'label': 'Option 1',
          'value': 'option-1'
        },
        {
          'label': 'Option 2',
          'value': 'option-2'
        }
      ],
      'pollAllowed': true
    },
    {
      'type': 'autocomplete',
      'icon': 'fa-bars',
      'label': 'Select',
      'description': 'Select',
      'placeholder': 'Select',
      'className': 'form-control',
      'values': [
        {
          'label': 'Option 1',
          'value': 'option-1'
        },
        {
          'label': 'Option 2',
          'value': 'option-2'
        },
        {
          'label': 'Option 3',
          'value': 'option-3'
        }
      ]
    },
    {
      'type': 'file',
      'icon': 'fa-file',
      'label': 'File Upload',
      'className': 'form-control',
      'subtype': 'file'
    },
    {
      'type': 'button',
      'icon': 'fa-paper-plane',
      'subtype': 'submit',
      'label': 'Submit'
    }
  ];

  pollFields = this.fieldModels.filter(fieldModel => fieldModel.pollAllowed);

  modelFields: Array<field> = [];
  model: any = {
    name: '',
    description: '',
    tags: [],
    theme: {
      bgColor: 'ffffff',
      textColor: '555555',
      bannerImage: ''
    },
    attributes: this.modelFields,
    type: 'form',
    customFields: {}
  };

  params: any;
  user: firebase.User;
  @ViewChild('tagsInput') tagsInput: ElementRef;
  deletedTags: string[] = [];
  newTags: any[];

  metadataFields: any[];

  constructor(
    private route: ActivatedRoute,
    private db: AngularFirestore,
    public auth: AuthService,
    private router: Router,
    public util: UtilService
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.params = params;
      this.db.doc(`tyro/${this.params['org']}/forms/metadataFields`).get()
        .subscribe(fieldsDoc => {
          // This creates fields for any extra fields created in the tyro/<org>/forms/metadataFields document.
          this.metadataFields = Object.keys(fieldsDoc.data());
          for (const fieldName of this.metadataFields) {
            this.model['customFields'][fieldName] = fieldsDoc.data()[fieldName];
            // Convert firestore Timestamps to dates
            if (this.model['customFields'][fieldName] instanceof firebase.firestore.Timestamp) {
              this.model['customFields'][fieldName] = this.model['customFields'][fieldName].toDate();
            }
          }
        });
      if (params['form']) {
        this.db.doc(`tyro/${this.params['org']}/forms/${this.params['form']}`).get()
          .subscribe((doc) => {
            // If form exists (which it always should), set up the model.
            if (doc.exists) {
              this.model = doc.data();
              if (!this.model.tags) {
                this.model.tags = [];
              }
            }
          });
      }
    });
    this.auth.user.subscribe((user: firebase.User) => {
      if (user) {
        this.db.doc(`tyro/${this.params['org']}/users/${user.uid}`).get()
          .subscribe((doc) => {
            if (doc.exists) {
              this.user = user;
            }
          });
      } else {
        this.router.navigateByUrl(`/organization/${this.params['org']}/login`);
      }
    });
  }

  onDragStart(event: DragEvent) {
    console.log('drag started', JSON.stringify(event, null, 2));
  }

  onDragEnd(event: DragEvent) {
    console.log('drag ended', JSON.stringify(event, null, 2));
  }

  onDragged(item: any, list: any[], effect: DropEffect) {
    if (effect === 'move') {
      const index = list.indexOf(item);
      list.splice(index, 1);
    }
  }

  onDragCanceled(event: DragEvent) {
    console.log('drag cancelled', JSON.stringify(event, null, 2));
  }

  onDragover(event: DragEvent) {
    console.log('dragover', JSON.stringify(event, null, 2));
  }

  onDrop(event: DndDropEvent, list?: any[]) {
    if (list && (event.dropEffect === 'copy' || event.dropEffect === 'move')) {

      if (event.dropEffect === 'copy') {
        event.data.name = event.data.type + '-' + new Date().getTime();
      }
      let index = event.index;
      if (typeof index === 'undefined') {
        index = list.length;
      }
      list.splice(index, 0, event.data);
    }
  }

  removeField(i) {
    swal({
      title: 'Are you sure?',
      text: 'Do you want to remove this field?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#00B96F',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, remove!'
    }).then((result) => {
      if (result.value) {
        this.model.attributes.splice(i, 1);
      }
    });

  }

  submit() {
    let valid = true;
    const validationArray = JSON.parse(JSON.stringify(this.model.attributes));
    validationArray.reverse().forEach(_field => {
      console.log(_field.label + '=>' + _field.required + '=>' + _field.value);
      if (_field.required && !_field.value && _field.type !== 'checkbox') {
        swal('Error', 'Please enter ' + _field.label, 'error');
        valid = false;
        return false;
      }
      if (_field.required && _field.regex) {
        const regex = new RegExp(_field.regex);
        if (regex.test(_field.value) === false) {
          swal('Error', _field.errorText, 'error');
          valid = false;
          return false;
        }
      }
      if (_field.required && _field.type === 'checkbox') {
        if (_field.values.filter(r => r.selected).length === 0) {
          swal('Error', 'Please enterrr ' + _field.label, 'error');
          valid = false;
          return false;
        }

      }
    });
    if (!valid) {
      return false;
    }
    console.log('Save', this.model);
    const input = new FormData;
    input.append('formId', this.model._id);
    input.append('attributes', JSON.stringify(this.model.attributes));
  }

  public async saveToFirebase() {
    console.log(this.model);
    const formItem = {
      'reference': undefined,
      'humanReadableName': this.model.name
    };
    let promises = [];
    if (!this.params || !this.params.form) {
      this.model['createdBy'] = {
        'reference': this.db.doc(`tyro/${this.params['org']}/users/${this.user.uid}`).ref,
        'humanReadableName': this.user.displayName || this.user.email
      };
      promises.push(
        this.db.collection(`tyro/${this.params['org']}/forms`).add(this.model)
          .then((doc: DocumentReference) => {
            formItem['reference'] = doc;
            this.db.doc(`tyro/${this.params['org']}/users/${this.user.uid}`)
              .update({'createdForms': firebase.firestore.FieldValue.arrayUnion(formItem)});
          }));
    } else {
      formItem['reference'] = this.db.doc(`tyro/${this.params['org']}/forms/${this.params['form']}`).ref;
      promises.push(this.db.doc(`tyro/${this.params['org']}/forms/${this.params['form']}`).update(this.model));
    }
    Promise.all(promises)
      .then(() => {
        promises = this.model.tags.map(tag => {
          this.db.doc(`tyro/${this.params['org']}/tags/${tag}`).update({
            forms: FieldValue.arrayUnion(formItem)
          });
        });
        Promise.all(promises)
          .then(() => {
            promises = this.deletedTags.map(tag => {
              this.db.doc(`tyro/${this.params['org']}/tags/${tag}`).update({
                forms: FieldValue.arrayRemove(formItem)
              });
            });
            Promise.all(promises)
              .then(() => {
                this.deletedTags = [];
                promises = this.newTags.map(tag => {
                  this.db.doc(`tyro/${this.params['org']}/tags/${tag}`).set({
                    forms: [formItem]
                  });
                });
                Promise.all(promises)
                  .then(() => {
                    this.router.navigateByUrl(`/organization/${this.params['org']}/form/${formItem['reference'].id}`);
                  });
              });
          });
      });
  }

  switchMode() {
    this.model.attributes = [];
  }
}
