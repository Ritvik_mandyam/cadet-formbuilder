import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {value} from '../global.model';

@Component({
  selector: 'app-form-field',
  templateUrl: './form-field.component.html',
  styleUrls: ['./form-field.component.css']
})
export class FormFieldComponent implements OnInit {
  @Input() item: any;
  @Input() index: number;
  @Output() remove = new EventEmitter();
  value: value = {
    label: '',
    value: ''
  };
  constructor() { }

  ngOnInit() {
  }

  addValue() {
    this.item.values.push(this.value);
    this.value = {label: '', value: ''};
  }
}
