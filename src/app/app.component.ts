import {Component} from '@angular/core';
import {AuthService} from './common/auth.service';
import * as firebase from 'firebase';
import {AngularFirestore} from '@angular/fire/firestore';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'formbuilder';

  constructor(public auth: AuthService, private db: AngularFirestore, private router: Router) {
  }

  activate() {
    this.auth.user.subscribe((user: firebase.User) => {
      if (user) {
        this.db.doc(`tyro/${/\/organization\/(.*?)\//.exec(this.router.url)[1]}/users/${user.uid}`).get()
          .subscribe((doc) => {
            if (!doc.exists) {
              this.router.navigateByUrl(`/organization/${/\/organization\/(.*?)\//.exec(this.router.url)[1]}/login`);
            } else {
              // User is logged in and exists in database.
              if (!doc.data()['isAdmin']) {
                this.router.navigateByUrl(`/organization/${/\/organization\/(.*?)\//.exec(this.router.url)[1]}/login`);
              }
            }
          });
      } else {
        // User is not logged in
        this.router.navigateByUrl(`/organization/${/\/organization\/(.*?)\//.exec(this.router.url)[1]}/login`);
      }
    });
  }
}
